//importing computer objects
import {computers} from "./computers.js"

// All buttons and the select form retrieved by id
const workButton = document.getElementById("work-btn");
const bankButton = document.getElementById("bank-btn");
const getLoanButton = document.getElementById("get-loan-btn");
const repayLoanButton = document.getElementById("loan-btn");
const lapTopSelect = document.getElementById("laptopId");
const buyButton = document.getElementById("buy-btn");

//all the fields to manipulate retrieved by id
const priceTag = document.getElementById("price-tag");
const displayCard = document.getElementById("display-card")
const descriptionText = document.getElementById("description-text")
const laptopTitle = document.getElementById("laptop-title");
const image = document.getElementById("img"); 
const featuresField = document.getElementById("features-field"); 
const loanText = document.getElementById("loan-text");
const paySum = document.getElementById("paysum"); 
const bankSum = document.getElementById("bank-balance"); 
const loanSum = document.getElementById("loan-sum");


let hasALoan = false; 

let payBalance = parseInt(paySum.innerHTML);
let bankBalance = 0; 
let outstandingLoan = 0; 

//The workbutton. 
//if clicked it increases the current pay balance by 100. 
workButton.addEventListener("click", () => {
    payBalance = parseInt(paySum.innerHTML);
    payBalance +=100;
    paySum.innerHTML=payBalance;
} )


//bankbutton. Banks paybalance to account balance. 
//if no loan is active it just banks the entire ammount in pay to account balance.
bankButton.addEventListener("click", () => {
    if(!hasALoan){
        payBalance = parseInt(paySum.innerHTML);
        bankBalance += payBalance;
        bankSum.innerHTML = bankBalance;
        payBalance=0;
        paySum.innerHTML = 0;
    }
    //if the user has a loan. the bank button will bank 10% of the ammount to the loan
    //90% goes to the bank account.
    else{
        let tax = payBalance * 0.10
        outstandingLoan -= tax;
        loanSum.innerHTML = outstandingLoan;

        bankBalance += payBalance-tax;
        bankSum.innerHTML = bankBalance;
        payBalance=0;
        paySum.innerHTML = 0;
        
        //checks if the loan is payed down. if it is all funcionality and information regarding current loan is removed
        if(outstandingLoan<=0){
            hideLoan();
        }

    }
});

//get loan button. 
//checks if user already has a loan, if it do, an error message is shown. 
getLoanButton.addEventListener("click", () => {
    if(hasALoan) {
        $('#modal3').modal("show");
    }else{
    //Lets the user pass in an ammount to loan. 
    let loanAmmount = prompt("Enter Ammount");
    //handles case where user dont enter anything.
    if (loanAmmount !== "") {
    let loanAmmountInteger =parseInt(loanAmmount);
    //checks if desired ammount is legal. if not error message is shown. 
    if(loanAmmountInteger> (bankBalance*2) ) {
        $('#modal2').modal("show");
    }else {
        //if legal loan information is shown. 
        loanText.innerHTML = "Outstanding loan"
        loanSum.innerHTML = loanAmmountInteger;
        outstandingLoan = loanAmmountInteger;
        repayLoanButton.style.display = "inline";
        hasALoan = true;
    }
}
}

})

//Repay loan button. 
repayLoanButton.addEventListener("click", ()=> {
    let loanBalance = parseInt(loanSum.innerHTML);
    payBalance = parseInt(paySum.innerHTML);
    //if loan balance is bigger than paybalance it means that not the entire loan will be downpayed
    //reducing the paybalance by the ammount currently in paybalance.
    //reducing the outstanding loan with the ammount of paybalance.
    if(loanBalance>payBalance) {
        loanSum.innerHTML = loanBalance-payBalance;
        paySum.innerHTML = 0;
        payBalance = 0;  
    //if paybalance is greater og equal to loanbalance. the paybalance will be reduced by the ammount it pays back
    //fires hideloan because loan is now downpayed. 
    }else{
        loanSum.innerHTML = 0;
        paySum.innerHTML = payBalance-loanBalance;
        payBalance=0;
        hideLoan();
    }

} )

//helper function to remove all loan information
const hideLoan = () => {
    hasALoan = false;
    repayLoanButton.style.display = "none";
    loanText.innerHTML = ""
    loanSum.innerHTML = "";
}

//select box. Updates information regarding what is selected in the select box.
lapTopSelect.addEventListener("change", ()=> {
    displayCard.style.display ="block";
    featuresField.innerHTML = computers[lapTopSelect.value].features;
    image.src = computers[lapTopSelect.value].image
    laptopTitle.innerHTML = computers[lapTopSelect.value].name
    descriptionText.innerHTML = computers[lapTopSelect.value].description
    priceTag.innerHTML = computers[lapTopSelect.value].price + " NOK"
})

//Buy button. If the user has sufficient funds in bank account it will be able to buy a computer
//displays a message accordingly
buyButton.addEventListener("click", () => {
    let price = parseInt(priceTag.innerHTML); 
    if(bankBalance>= price) {
        $('#modal1').modal("show");
        bankBalance= bankBalance - price; 
        bankSum.innerHTML = bankBalance;
        hasALoan = false;
    }else{
        $('#modal4').modal("show");
    }
})


