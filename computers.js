// List of computer objects with unique information.
export const computers = [
    {
        id: "1",
        name : "MacBook Air 13",
        features: "Apple 8-Core M1 CPU, 8GB RAM, 256GB SSD, Apple 7-Core GPU",
        image : "/images/macbook.png",
        description :"Dont be scared by the price, you get an apple on your computer!!",
        price : "5000"
    },
    {
        id: "2",
        name : "Razer Blade 15",
        features: "GeForce RTX 2070 Super, Core i7-10875H, 16 GB RAM, 512 GB SSD, Windows 10 Home",
        image : "images/razer.png",
        description :"You can only use this computer if you have a gaming chair",
        price : "3000"
    },
    {   
        id: "3",
        name : "Lenovo IdeaPad Gaming",
        features: "GeForce GTX 1650, Ryzen 5 4600H, 8 GB RAM, 512 GB SSD, Windows 10 Home",
        image : "images/lenovo.png",
        description :"1650 card?? you should go for the razer computer instead. However if you want to game like it was 2010, this is for you:)",
        price : "2000"
    },
    {
        id: "4",
        name : "Acer Chromebook 315",
        features: "Pentium N5030 Quad Core, 8 GB RAM, 128 GB SSD, Google Chrome OS",
        image : "images/chromebook.png",
        description :"Yes yes it has silver color, but its not a mac!",
        price : "1000"
    }
]